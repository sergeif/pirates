module Main where

import System.IO
import Control.Monad
import System.IO.Unsafe  -- be careful!                                         
import System.Random
import Data.List
import Data.Maybe
import Control.Monad.IO.Class

data Barrel = Barrel { xb :: Int, yb :: Int, rumInBarrel :: Int } deriving (Show,Eq)
data Cannonball = Cannonball { xc :: Int, yc :: Int, timec :: Int, ownerId :: Int } deriving Show
data Mine = Mine {xm :: Int, ym :: Int} deriving (Show,Eq)
data Ship = Ship { xs :: Int, ys :: Int, speeds :: Int, orientation :: Int, shipTeam :: Int, shipId :: Int, rumAmount :: Int, collectedMines :: [Mine], collectedBarrels :: [Barrel]} deriving (Show,Eq)
data World = World { getShips :: [Ship], getMines :: [Mine], getCannonballs :: [Cannonball], getBarrels :: [Barrel], turn :: Int, getNearestBarrel :: [Barrel], getNearestShip :: [Ship] }
data Action = Wait | Faster | Slower | Port | Starboard 
 deriving (Show,Eq)

data GameState = GameState { getCooldowns :: [(Int, Int)] }

setTurn :: World -> Int -> World
setTurn w t = World (getShips w) (getMines w) (getCannonballs w) (getBarrels w) t (getNearestBarrel w) (getNearestShip w)

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering -- DO NOT REMOVE
    let gameState = GameState []
    loop gameState

parseBarrels :: [String] -> [Barrel]
parseBarrels [] = []
parseBarrels (s:xs) = if (entitytype == "BARREL") then (Barrel x y rum):(parseBarrels xs) else parseBarrels xs
 where
  ss = words s
  entitytype = ss!!1
  x = read (ss!!2) :: Int
  y = read (ss!!3) :: Int
  rum = read (ss!!4) :: Int

parseCannonBalls :: [String] -> [Cannonball]
parseCannonBalls [] = []
parseCannonBalls (s:xs) = if (entitytype == "CANNONBALL" && t > 0) then (Cannonball x y t o):(parseCannonBalls xs) else parseCannonBalls xs
 where
  ss = words s
  entitytype = ss!!1
  x = read (ss!!2) :: Int
  y = read (ss!!3) :: Int
  o = read (ss!!4) :: Int -- owner id
  t = read (ss!!5) :: Int -- time he number of turns before impact (1 means the cannon ball will land at the end of the turn

parseMines :: [String] -> [Mine]
parseMines [] = []
parseMines (s:xs) = if (entitytype == "MINE") then (Mine x y):(parseMines xs) else parseMines xs
 where
  ss = words s
  entitytype = ss!!1
  x = read (ss!!2) :: Int
  y = read (ss!!3) :: Int
  
parseMyShips :: [String] -> [Ship]
parseMyShips [] = []
parseMyShips (s:xs) = if (entitytype == "SHIP" && team == 1) then (Ship x y speed o team entId rum [] []):(parseMyShips xs) else parseMyShips xs
 where
  ss = words s
  entId = read (ss!!0) :: Int
  entitytype = ss!!1
  team = read (ss!!7) :: Int
  x = read (ss!!2) :: Int
  y = read (ss!!3) :: Int
  o = read (ss!!4) :: Int
  speed = read (ss!!5) :: Int
  rum = read (ss!!6) :: Int

parseEnemyShips :: [String] -> [Ship]
parseEnemyShips [] = []
parseEnemyShips (s:xs) = if (entitytype == "SHIP" && team == 0) then (Ship x y speed o team entId rum [] []):(parseEnemyShips xs) else parseEnemyShips xs
 where
  ss = words s
  entId = read (ss!!0) :: Int
  entitytype = ss!!1
  team = read (ss!!7) :: Int
  x = read (ss!!2) :: Int
  y = read (ss!!3) :: Int
  speed = read (ss!!5) :: Int
  o = read (ss!!4) :: Int
  rum = read (ss!!6) :: Int

distS2Pos :: (Int,Int,Int) -> (Int,Int) -> Int
distS2Pos (x,y,o) (x2,y2) = max (abs $ x2 - x) (abs $ y2-y)

distC2Pos :: (Int,Int) -> (Int,Int) -> Int
distC2Pos (x,y) (x2,y2) = max (abs $ x2 - x) (abs $ y2-y)

distS2S :: Ship -> Ship -> Int
distS2S s1 s2 = ((abs $ aq-bq) + (abs $ aq+ar-bq-br) + (abs $ ar-br)) `div` 2
 where
  x1 = xs s1
  x2 = xs s2
  y1 = ys s1
  y2 = ys s2
  aq = x1 - (y1 + y1 `mod` 2) `div` 2
  ar = y1
  bq = x2 - (y2 + y2 `mod` 2) `div` 2
  br = y2

angle :: Int -> Int -> Int -> Int -> Int
angle x1 y1 x2 y2 = fromIntegral $ floor $ pi * atan2 ((fromIntegral y2)-(fromIntegral y1)) ((fromIntegral x2)-(fromIntegral x1)) / 180.0

distS2N :: Ship -> Barrel -> Int
distS2N sh b = ((abs $ aq-bq) + (abs $ aq+ar-bq-br) + (abs $ ar-br)) `div` 2
 where
  x1 = xs sh
  x2 = xb b
  y1 = ys sh
  y2 = yb b
  aq = x1 - (y1 + y1 `mod` 2) `div` 2
  ar = y1
  bq = x2 - (y2 + y2 `mod` 2) `div` 2
  br = y2
  
findNearestBarrel :: Ship -> [Barrel] -> Barrel
findNearestBarrel sh bs = head $ sortBy f bs
 where
  f b1 b2 = compare (distS2N sh b1) (distS2N sh b2)

findNearestShip :: Ship -> [Ship] -> Ship
findNearestShip sh es = head $ sortBy f es
 where
  f b1 b2 = compare (distS2S sh b1) (distS2S sh b2)

evenHexMove (x,y) 0 speed = (x+speed,y)
evenHexMove (x,y) 1 speed = (x,y-speed)
evenHexMove (x,y) 2 speed = (x-speed,y-speed)
evenHexMove (x,y) 3 speed = (x-speed,y)
evenHexMove (x,y) 4 speed = (x-speed,y+speed)
evenHexMove (x,y) 5 speed = (x,y+speed)

oddHexMove (x,y) 0 speed = (x+speed,y)
oddHexMove (x,y) 1 speed = (x+speed,y-speed)
oddHexMove (x,y) 2 speed = (x,y-speed)
oddHexMove (x,y) 3 speed = (x-speed,y)
oddHexMove (x,y) 4 speed = (x,y+speed)
oddHexMove (x,y) 5 speed = (x+speed,y+speed)

calcCoordinates :: (Int,Int) -> Int -> Int -> (Int,Int)
calcCoordinates (x,y) orient speed | y `mod` 2 == 0 = evenHexMove (x,y) orient speed 
                                   | otherwise = oddHexMove (x,y) orient speed

pointInShip s1 (qx,qy) = (x==qx&&y==qy)||(x1==qx&&y1==qy)||(x2==qx&&y2==qy)
 where
  (x,y,o) = (xs s1, ys s1, orientation s1)
  (x1,y1) = calcCoordinates (x,y) o 1
  (x2,y2) = calcCoordinates (x,y) ((o+3)`mod`6) 1

shipsCollide s1 s2 = (pointInShip s1 (qx,qy)) || (pointInShip s1 (qx1,qy1)) || (pointInShip s1 (qx2,qy2))  -- || (pointInShip s1 (qx3,qy3))
 where
  (qx,qy,qo) = (xs s2, ys s2, orientation s2)
  (qx1,qy1) = calcCoordinates (qx,qy) qo 1
  (qx2,qy2) = calcCoordinates (qx,qy) ((qo+3)`mod`6) 1
  --(qx3,qy3) = calcCoordinates (qx,qy) qo 2

isValidPos :: (Int,Int,Int) -> World -> Bool
isValidPos (x,y,o) w = x >= 0 && x < 23 && y>=0 && y<21

isValidShip s w = isValidPos (xs s,ys s,orientation s) w && (not $ any (\t->((shipsCollide s t)||(shipsCollide t s)) && (shipId s /= shipId t)) (getShips w))

--data Ship = Ship { xs :: Int, ys :: Int, speeds :: Int, orientation :: Int, shipTeam :: Int, shipId :: Int, rumAmount :: Int, collectedMines :: [Mine], collectedBarrels :: [Barrel]} deriving (Show,Eq)
shipInteraction :: Int -> World -> Ship -> Ship
shipInteraction rumAdd w (Ship x y s o t i r cms cbs)  = Ship x y s o t i rum cms' cbs'
 where
  (x1,y1) = calcCoordinates (x,y) o 1
  (x2,y2) = calcCoordinates (x,y) ((o+3)`mod`6) 1
  cs = getCannonballs w
  barrels = filter (\b->((xb b)==x&&(yb b)==y)||((xb b)==x1&&(yb b)==y1)||((xb b)==x2&&(yb b)==y2)) $ filter (\b->not $ elem b cbs) (getBarrels w)
  mines = filter (\b->((xm b)==x&&(ym b)==y)||((xm b)==x1&&(ym b)==y1)||((xm b)==x2&&(ym b)==y2)) $ filter (\m -> not $ elem m cms) (getMines w)
  ballsCenter = filter (\c->(1 >= (abs $ timec c - turn w))&&(xc c)==x&&(yc c)==y) cs
  ballsAround = filter (\c->(1 > (abs $ timec c - turn w))&&(((xc c)==x1&&(yc c)==y1)||((xc c)==x2&&(yc c)==y2)) ) cs
  rum = r + rumAdd + (sum $ map rumInBarrel barrels) - 25*(length mines) - 25 * (length ballsAround) - 50 * (length ballsCenter)
  cms' = mines ++ cms
  cbs' = barrels ++ cbs

setSpeed :: Ship -> Int -> Ship
setSpeed (Ship x y s o t i r cms cbs) ns  = Ship x y ns o t i r cms cbs

moveShipHelper :: World -> Ship -> Int -> Ship
moveShipHelper w (Ship x y s o t i r cms cbs) 1 = let (nx,ny) = calcCoordinates (x,y) o 1 
                                                  in Ship nx ny s o t i r cms cbs
moveShipHelper w s 2 = moveShipHelper w (moveShipHelper w s 1) 1
moveShipHelper w s _ = s

  
rotateShipHelper :: World -> Ship -> Int -> Ship
rotateShipHelper w (Ship x y s o t i r cms cbs) ochange = Ship x y s ((o + ochange) `mod` 6 ) t i r cms cbs
   
moveShip :: World -> Ship -> Action -> Maybe Ship
moveShip w ship@(Ship x y s o t i r cms cbs) Wait | rum < 0 = Nothing
                                                  | otherwise = Just s''
 where 
  ss' = map (\t -> moveShipHelper w ship t) [1..s]
  s' = last $ ship : (takeWhile (\q -> isValidShip q w) ss')
  ns = if length ss' > 0 && isValidShip (last ss') w then s else 0
  (nx,ny) = (xs s', ys s')
  s'' = shipInteraction (-1) w (Ship nx ny ns o t i r cms cbs)
  rum = min 100 $ rumAmount s''
moveShip w ship@(Ship x y s o t i r cms cbs) Faster | (s > 1)||(rum<0) = Nothing
                                                    | otherwise = Just s''
 where
  ss' = map (\t -> moveShipHelper w ship t) [1..(1+s)]
  s' = last $ ship : (takeWhile (\q -> isValidShip q w) ss')
  ns = if length ss' > 0 && isValidShip (last ss') w then s + 1 else 0
  (nx,ny) = (xs s', ys s')
  s'' = shipInteraction (-1) w (Ship nx ny ns o t i r cms cbs)
  rum = min 100 $ rumAmount s''
moveShip w ship@(Ship x y s o t i r cms cbs) Slower | (s < 1)||(rum < 0) = Nothing
                                                    | otherwise = Just s''
 where
  ss' = map (\t -> moveShipHelper w ship t) [1..(s-1)]
  s' = last $ ship : (takeWhile (\q -> isValidShip q w) ss')
  ns = if length ss' > 0 && isValidShip (last ss') w then s - 1 else 0
  (nx,ny) = (xs s', ys s')
  s'' = shipInteraction (-1) w (Ship nx ny ns o t i r cms cbs)
  rum = min 100 $ rumAmount s''
moveShip w ship@(Ship x y s o t i r cms cbs) Port | rum < 0 = Nothing
                                                  | otherwise = Just s''
 where
  ss' = map (\t -> moveShipHelper w ship t) [0..s]
  ss'' = if length ss' > 0 then ss' ++ [rotateShipHelper w (last ss') 1] else []
  s' = last $ ship : (takeWhile (\q -> isValidShip q w) ss'')
  ns = if length ss'' > 0 && isValidShip (last ss'') w then (speeds s')  else 0
  no = if length ss'' > 0 && isValidShip (last ss'') w then (orientation s') else o
  (nx,ny) = (xs s', ys s')
  s'' = if (o /= no) then shipInteraction 0 w $ rotateShipHelper w (shipInteraction (-1) w (Ship nx ny ns o t i r cms cbs)) 1 else shipInteraction (-1) w (Ship nx ny ns o t i r cms cbs)
  rum = min 100 $ rumAmount s''
moveShip w ship@(Ship x y s o t i r cms cbs) Starboard | rum < 0 = Nothing
                                                       | otherwise = Just s''
 where
  ss' = map (\t -> moveShipHelper w ship t) [0..s]
  ss'' = if length ss' > 0 then ss' ++ [rotateShipHelper w (last ss') 5] else []
  s' = last $ ship : (takeWhile (\q -> isValidShip q w) ss'')
  ns = if length ss'' > 0 && isValidShip (last ss'') w then (speeds s')  else 0
  no = if length ss'' > 0 && isValidShip (last ss'') w then (orientation s') else o
  (nx,ny) = (xs s', ys s')
  s'' = if (o /= no) then shipInteraction 0 w $ rotateShipHelper w (shipInteraction (-1) w (Ship nx ny ns o t i r cms cbs)) 5 else shipInteraction (-1) w (Ship nx ny ns o t i r cms cbs)
  rum = min 100 $ rumAmount s''


evolution :: World -> [([Action],Maybe Ship)] -> [([Action],Maybe Ship)]
evolution w xs = [ ((a:ls), moveShip w s a) | a <- [Faster, Wait, Slower, Port, Starboard], (ls,Just s) <- xs]

brutforce :: World -> [([Action],Maybe Ship)] -> [([Action],Maybe Ship)]
brutforce w xs = evolution (setTurn w 5) $ evolution (setTurn w 4) $ evolution (setTurn w 3) $ evolution (setTurn w 2) $ xs

scoreShip :: World -> ([Action],Maybe Ship) -> Int
scoreShip _ (as,Nothing) = (-1)
scoreShip w (as, Just s) = 40 * (((rumAmount s) * 25) - minesBehind*5 - (scorePosition (xs s) (ys s) (orientation s)) - (scoreAction as)) - scoreBarrelDistance + scoreShipDistance
 where
  scoreAction as | (speeds s ==0) && (last as == Wait) = 15
                 | last as == Wait = 1
                 | last as == Slower = 1
                 | otherwise = 0              
  scorePosition x y o | (x <= 1 && o >= 2 && o <= 4) = 8
                      | (x >= 21 && (o < 2 || o == 5)) = 8
                      | (y <= 1 && o >= 1 && o <= 2) = 8
                      | (y >= 19 && o >= 4 && o <= 5) = 8
                      | (x == 0 || y == 0 || x == 22 || y == 20) = 3
                      | (x == 1 || y == 1 || x == 21 || y == 19) = 1
                      | otherwise = 0
  barrelDistance = head $ map (\b -> distS2N s b) (getNearestBarrel w)
  scoreBarrelDistance = if barrelDistance > 5 then (barrelDistance - 4) else 0
  scoreShipDistance = minimum $ map (\b -> distS2Shot s b) (getNearestShip w)
  (xn,yn) = calcCoordinates (calcCoordinates (xs s,ys s) (orientation s) 1) (orientation s) 1
  minesBehind = length $ filter (\m -> (xm m == xn && ym m == yn)) (getMines w)

filterEvo xs = [q | q@(as, Just s) <- xs]


bestActions :: World -> [([Action],Maybe Ship)] -> [Action]
bestActions w xs = reverse $ snd $ head $ sortBy (\a b -> compare (fst b) (fst a)) [(scoreShip w q, as) | q@(as, Just s) <- xs]

shipEvolution :: World -> [Action] -> Maybe Ship -> Maybe Ship
shipEvolution _ _ Nothing = Nothing
shipEvolution w [] s = s
shipEvolution w (a:as) (Just s) = shipEvolution w as (moveShip w s a)

printAction :: Action -> String
printAction Wait = "WAIT"
printAction Port = "PORT"
printAction Faster = "FASTER"
printAction Starboard = "STARBOARD"
printAction Slower = "SLOWER"

updateCooldowns :: [(Ship,(String,Int))] -> [(Int,Int)]
updateCooldowns [] = []
updateCooldowns (x:xs) = (shipId $ fst x, snd $ snd x) : (updateCooldowns xs)

badPosition :: Ship -> Bool
badPosition s = (speeds s == 0)&&((xs s==0)||(ys s==0)||(xs s==22)||(ys s>=20))

bestActionsOut w xs = take 10 $ sortBy (\a b -> compare (fst b) (fst a)) [(scoreShip w q, as) | q@(as, Just s) <- xs]

canFire :: World -> Ship -> Bool
canFire w s = (s' /= Nothing) && ((rum s') > (rumAmount s) - 3)
 where
  s' = shipEvolution w [Wait, Wait] (Just s)
  rum Nothing = 0
  rum (Just s'') = rumAmount s''
  
canMove :: World -> Ship -> Bool
canMove w s = s' /= Nothing && speeds (fromJust s') > 0 
 where
  s' = shipEvolution w [Faster] (Just s)
  
needMove :: World -> Ship -> Maybe Ship -> Bool
needMove w s Nothing = True
needMove w s (Just ns) = (rumAmount ns) < (rumAmount s)

distS2ShotPnt :: Ship -> (Int,Int) -> Int
distS2ShotPnt s1 (x2,y2) = ((abs $ aq-bq) + (abs $ aq+ar-bq-br) + (abs $ ar-br)) `div` 2
 where
  (x1,y1) = calcCoordinates (xs s1, ys s1) (orientation s1) 1
  aq = x1 - (y1 + y1 `mod` 2) `div` 2
  ar = y1
  bq = x2 - (y2 + y2 `mod` 2) `div` 2
  br = y2
  
moveShips :: World ->  [Ship] -> [[Ship]]
moveShips _ [] = []
moveShips w (s:xs) = [fromJust s' | a <- [Faster, Wait, Slower, Port, Starboard], s' <- [moveShip w s a], s' /= Nothing] : (moveShips w xs)

moveShips2 :: World ->  [Ship] -> [[Ship]]
moveShips2 _ [] = []
moveShips2 w (s:xs) = [fromJust s'' | a <- [Faster, Wait, Slower, Port, Starboard], b <- [Faster, Wait, Slower, Port, Starboard], s' <- [moveShip w s a], s' /= Nothing, s'' <- [moveShip w (fromJust s') b], s'' /= Nothing] : (moveShips2 w xs)

distS2Shot :: Ship -> Ship -> Int
distS2Shot s1 s2 = ((abs $ aq-bq) + (abs $ aq+ar-bq-br) + (abs $ ar-br)) `div` 2
 where
  (x1,y1) = calcCoordinates (xs s1, ys s1) (orientation s1) 1
  (x2,y2) = (xs s2, ys s2)
  aq = x1 - (y1 + y1 `mod` 2) `div` 2
  ar = y1
  bq = x2 - (y2 + y2 `mod` 2) `div` 2
  br = y2

scorePoint :: World -> Ship -> (Int, Int) -> Int
scorePoint w s (x,y) = 25 * (length enemiesCenters) + 25 * (length enemiesAround) - 100 * (length allyShips)
 where
  enemies = filter (\t -> (shipTeam t) /= (shipTeam s)) (getShips w)
  allies = filter (\t -> (shipTeam t) == (shipTeam s)) (getShips w)
  enemiesCenters = filter (\t -> (xs t == x) && (ys t == y)) enemies
  enemiesAround = filter (\t -> pointInShip t (x,y)) enemies
  allyShips = filter (\t -> pointInShip t (x,y)) allies

goodShots :: World -> [Ship] -> Ship -> [((Int, Int),Int)]
goodShots w ss s = sortBy (\a b -> compare (snd b) (snd a)) pnts
  where
   pnts = [((x,y),scorePoint w s (x,y)) | x <- [0..22], y <- [0..20], distS2ShotPnt s (x,y) < 3]



goodShots2 :: World -> ([Barrel],[Barrel]) -> [Ship] -> Ship -> [((Int, Int),Int)]
goodShots2 w (ecbs, mcbs) ss s = sortBy (\a b -> compare (snd b) (snd a)) pnts
  where
   pnts = [((x,y),scorePoint' (x,y)) | x <- [0..22], y <- [0..20], distS2ShotPnt s (x,y) >= 3, distS2ShotPnt s (x,y) < 5]
   scorePoint' (x,y) = enemyBarrels (x, y)
   enemyBarrels (x,y) = let (e,m) = (length $ filter (\b -> (xb b == x)&&(yb b == y)) ecbs, length $ filter (\b -> (xb b == x)&&(yb b == y)) mcbs)
                        in if (e > m)&&(m==0) then e else 0

hasSomeoneBehind :: Ship -> [Ship] -> Bool
hasSomeoneBehind s es = any (\t -> (not $ pointInShip t p1) && ((pointInShip t p2) || (pointInShip t p3) || (pointInShip t p4) )) es
 where
  p = (xs s, ys s)
  o = (3 + (orientation s))`mod`6
  p0 = calcCoordinates p o 1
  p1 = calcCoordinates p0 o 1
  p2 = calcCoordinates p0 ((o+1)`mod`6) 1
  p3 = calcCoordinates p0 ((o+5)`mod`6) 1
  p4 = calcCoordinates p1 o 1


   
loop :: GameState -> IO ()
loop gameState = do
    input_line <- getLine
    let myshipcount = read input_line :: Int -- the number of remaining ships
    input_line <- getLine
    let entitycount = read input_line :: Int -- the number of entities (e.g. ships, mines or cannonballs)
    entities <- replicateM entitycount getLine :: IO [String]
    
    let mines = parseMines entities
    --hPutStrLn stderr $ show mines
    let cannonballs = parseCannonBalls entities
    let preBarrels = parseBarrels entities
    let myShips = parseMyShips entities
    let enemies = parseEnemyShips entities  
    let worldCheck = World (myShips++enemies) mines cannonballs preBarrels 1 [] []
    let shipsToCheck = concat $ moveShips worldCheck (getShips worldCheck) 
    let shipsToCheck2 = concat $ moveShips2 worldCheck (getShips worldCheck) 

    
    commands <- forM myShips $ \s -> do
        -- hPutStrLn stderr "Debug messages..."
        -- Any valid action, such as "WAIT" or "MOVE x y"
        let worldBefore = World ((filter (\q -> (shipId q) /= (shipId s)) myShips)++enemies) mines cannonballs preBarrels 1 [] []
        let enemyBarrels = nub $ concat $ map (\t -> if (shipTeam t /= shipTeam s) then collectedBarrels t else []) shipsToCheck2
        let myBarrels = nub $ concat $ map (\t -> if (shipTeam t == shipTeam s) then collectedBarrels t else []) shipsToCheck2
        
        let enemyShip = findNearestShip s enemies
        let enemyEvo = shipEvolution worldBefore [Wait,Wait] (Just enemyShip)
        let enemy = if (enemyEvo /= Nothing) then fromJust enemyEvo else enemyShip
        let barrels = if length preBarrels > 0 then preBarrels else [Barrel (xs enemyShip) (ys enemyShip) 0]
        let barrel = findNearestBarrel s barrels
        let nearestShips = enemyShip : filter (\t -> distS2S s t < 9) shipsToCheck
        let world = World (getShips worldBefore) mines cannonballs preBarrels 1 [barrel] nearestShips
        --hPutStrLn stderr $ show s
        let evo1 = filterEvo $ evolution (setTurn world 1) $ [([], Just s)]
        let evo2 = filterEvo $ evolution (setTurn world 2) $ evo1
        let evo3 = filterEvo $ evolution (setTurn world 3) $ evo2
        let evo4 = filterEvo $ evolution (setTurn world 4) $ evo3
        let bestEvo4 = take 20 $ sortBy (\a b -> compare (scoreShip world b) (scoreShip world a)) evo4
        let evo5 = filterEvo $ evolution (setTurn world 5) $ bestEvo4
        let evo = evo5
        --hPutStrLn stderr $ show $ bestActionsOut world evo 
        --hPutStrLn stderr $ show $ map (scoreShip world) (sortBy (\a b -> compare (fst b) (fst a)) evo)
        --hPutStrLn stderr $ show enemyEvo
        --hPutStrLn stderr $ show (moveShip world s Faster)
        let bestEvo = if length evo5 > 0 
                      then bestActions world evo5
                      else if length evo4 > 0
                          then bestActions world evo4
                          else if length evo3 > 0
                              then bestActions world evo3
                              else if length evo2 > 0
                                  then bestActions world evo2
                                  else if length evo1 > 0
                                         then bestActions world evo1
                                         else [Slower]
        --hPutStrLn stderr $ show bestEvo
        --hPutStrLn stderr $ show $ shipEvolution world bestEvo (Just s)
        --let shipEvo = shipEvolution world bestEvo (Just s)
        --let allEquals = length (nub bestEvo) == 1
        let closeShots = goodShots world shipsToCheck s
        let midShots = goodShots2 world (enemyBarrels,myBarrels) shipsToCheck2 s
        --hPutStrLn stderr $ show $ closeShots
        let cooldown = fromMaybe 0 (lookup (shipId s) (getCooldowns gameState))
        let command = if (cooldown == 0) && (canFire world s) && ((snd $ head closeShots) > 25)  then ("FIRE " ++ (show $ fst $ fst $ head $ closeShots) ++ " " ++ (show $ snd $ fst $ head $ closeShots), 1)
                      else if (1 > speeds enemy) && ((canFire world s)||(length bestEvo == 1)) && (distS2Shot s enemy) < 10 && cooldown == 0 then ("FIRE " ++ (show $ xs enemy) ++ " " ++ (show $ ys enemy), 1)
                      else if (speeds s > 0)&&(hasSomeoneBehind s enemies) then ("MINE",0)
                      else ((printAction $ head bestEvo),0)
                      -- else if (head bestEvo == Wait)&&(speeds s > 0) then ("MINE",0)
                      --else if (length bestEvo == 1)&&(speeds s > 0) then ("MINE",0)                      
        return command

    forM_ commands $ \s -> do
        putStrLn $ fst s
        return ()

    let newGameState = GameState (updateCooldowns $ zip myShips commands)
    
    loop newGameState